..  Atmelavr Examle Native (https://gitlab.com/itlernpfad_public/atmelavr-example-native)
    Modified 2021 by ITlernfad
    Based on platformio-examples (https://github.com/platformio/platformio-examples):
    
    Copyright 2014-present PlatformIO <contact@platformio.org>
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
       http://www.apache.org/licenses/LICENSE-2.0
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

How to build PlatformIO based project
=====================================

1. Install the PlatformIO IDE (https://docs.platformio.org)
    i) `Install PlatformIO IDE for VSCode <https://docs.platformio.org/en/latest//integration/ide/vscode.html#ide-vscode>`_
    ii) Install a `Git client <https://docs.platformio.org/en/latest//integration/ide/vscode.html#ide-vscode>`_
    iii) Run Microsoft Visual Studio Code with the PlatformIO IDE. Left-click "File -> Preferences -> Settings -> Extensions". In section "Git", left-click on link to settings.json 
    iv) in the file settings.json opened in step iii), verify if the entry for the key "git.path" specifies the correct path to the git executable. 
2. Install the Platform for the target
    i) Run Microsoft Visual Studio Code with the PlatformIO IDE. In the left VSCode Action toolbar, left-click "PlatformIO -> Quick Access -> Platforms -> Embedded". 
    ii) Use the filter to search for "Atmel AVR". 
    iii) Left-click on the platform "Atmel AVR -> Install".
    iv) Wait until installation has succeeded and follow the instructions on screen. 
3. Clone Atmelavr-Example-Native 
    i) Run Microsoft Visual Studio Code with the PlatformIO IDE. In the left toolbar, left-click "PlatformIO -> Quick Access -> Miscellaneous -> Clone Git Project". 
    ii) Type "https://gitlab.com/itlernpfad_public/atmelavr-example-native" and press the enter-key. 
    iii) Select a path for the local reposiory and confirm, then wait.
    iv) In the notification at the bottom right, left-click "Add to Workspace".
4. Initiate the build process
    i) In VSCode, open the Explorer view: In the left VSCode Action toolbar, left-click "Explorer".
    ii) In the Explorer panel of VSCode, left-click on the platformio.ini file at the root of the project "Atmel-avr-example-native".
    iii) Verify that platform.ini contains the correct content (for build target Arduino Uno Rev. 3, see below)
    iv) In the PlatformIO toolbar at the bottom (<https://docs.platformio.org/en/latest//integration/ide/vscode.html#ide-vscode-toolbar>), left-click on the central "Project Environment Switcher" and select the desired environment, e.g. "Default (atmelavr-example-native)". Note that project name in parentheses (here: atmelavr-example-native) must match your current project.
    v) In the PlatformIO toolbar at the bottom, left-click on the button "PlatformIO: Build" (alternatively, in the left VSCode Action toolbar, click "PlatformIO -> Project tasks -> General -> Default -> Build All).
    vi) In the PlatformIO toolbar at the bottom, left-click on the button "PlatformIO: Build" (alternatively, in the left VSCode Action toolbar, click "PlatformIO -> Project tasks -> General -> Default -> Upload All).

plaformio.ini for build target Arduino Uno Rev.3:

```
[platformio]

default_envs = uno
include_dir = src  

[env]

lib_deps = 
    https://gitlab.com/itlernpfad_public/stdio_setup.git

[common_uno]
platform = atmelavr
board = uno

upload_protocol = arduino

[env:uno]

extends = common_uno
build_type = release
build_flags = 
    ; compiler options for linking in an implementation of the printf function that supports floating point conversions.
    ; https://www.nongnu.org/avr-libc/user-manual/group__avr__stdio.html
    -Wl,-u,vfprintf
    -lprintf_flt
    -lm
```

License information
===================

```
Atmelavr Examle Native (https://gitlab.com/itlernpfad_public/atmelavr-example-native)
Modified 2021 by ITlernfad
Based on platformio-examples (https://github.com/platformio/platformio-examples):
    
Copyright 2014-present PlatformIO <contact@platformio.org>
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
   http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.`
```
