/* SPDX-License-Identifier: Apache-2.0 */

/**
 * Atmelavr Examle Native
 * Modified 2021 by ITlernfad
 * Based on platformio-examples (https://github.com/platformio/platformio-examples): 
 * Copyright (C) PlatformIO <contact@platformio.org>
 * See LICENSE for details.
 */

#include <avr/io.h> /* AVR device-specific IO definitions. https://www.nongnu.org/avr-libc/user-manual/group__avr__io.html */
#include <util/delay.h> /* provides _delay_ms(). https://www.nongnu.org/avr-libc/user-manual/group__util__delay.html */

/* un-comment the line below for debugging using printf statements. https://gitlab.com/itlernpfad_public/stdio_setup */
/* #define DEBUGGING */

#if defined(DEBUGGING) 
  #include <stdio.h> /* provides function printf */

  /**
   * Provides UartInit() to send stderr and stdio streams via UART. On Arduino Uno-compatible development boards, serial data from UART is sent via USB. 
   * https://gitlab.com/itlernpfad_public/stdio_setup 
   */
  #include "stdio_setup.h" 
#endif

int main(void)
{    
    #if defined(DEBUGGING)
      /**
       * send stderr and stdio streams via UART. On Arduino Uno-compatible development boards, serial data from UART is sent via USB. 
       * https://gitlab.com/itlernpfad_public/stdio_setup 
       */
      UartInit();
      printf("Hello World!");
    #endif

    /* make the LED pin an output for PORTB5 */
    DDRB |= (1 << 5);

    for(;;)
    {
        _delay_ms(500);

        /* flip PORTB5 bit to toggle the built-in LED of the Arduino Uno at I/O port pin PB5 */
        PORTB ^= (1 << 5);
    }

    return 0;
}
